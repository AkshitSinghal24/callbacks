const fs = require("fs");

function readAndRight(fileLocation, callback) {
  fs.readFile(fileLocation, (err, data) => {
    if (err) {
      console.log(err);
      return;
    } else {

      callback(data);
    }
  });
}

function convertUpper(data, callback) {
  fs.writeFile("upperCaseData.txt", data.toString().toUpperCase(), (err) => {
    if (err) {
      console.log(err);
      return;
    } else {
      console.log("UpperCaseFile created");
      fs.writeFile("./filenames.txt", "upperCaseData.txt", (err) => {
        if (err) {
          console.log(err);
          return;
        } else {
          console.log("UpperCaseFile name Added in Filenames.txt");
          fs.readFile("upperCaseData.txt", "utf8", (err, upperCaseData) => {
            if (err) {
              console.log(err);
              return;
            } else {
              callback(upperCaseData);
            }
          });
        }
      });
    }
  });
}

function convertLower(upperCaseData, callback) {
  upperCaseData = upperCaseData.toString().toLowerCase();
  upperCaseData = upperCaseData.split(".");
  let sentenceFile = "sentenceFile.txt";
  upperCaseData.forEach((line) => {
    fs.appendFile(sentenceFile, line + "\n", (err) => {
      if (err) {
        console.log(err);
        return;
      }
    });
  });
  console.log(`Created Sentence File`);
  fs.appendFile("./filenames.txt", "\n" + sentenceFile, (err) => {
    if (err) {
      console.log(err);
      return;
    } else {
      console.log("SentenceFile name Added in Filenames.txt");
      fs.readFile("./filenames.txt", "utf-8", (err, fileData) => {
        if (err) {
          console.log(err);
          return;
        } else {
          callback(fileData);
        }
      });
    }
  });
}

function sortFile(data, callback) {
  data = data.split("\n");
  fs.readFile(data[0], "utf-8", (err, content) => {
    if (err) {
      console.log(err);
      return;
    } else {
      content = content.split(" ");
      content.sort((a, b) => {
        if (a > b) return 1;
        else if (a < b) return -1;
        else return 0;
      });
      content = content.toString();
      sortFile = "sorted_file.txt";
      fs.writeFile(sortFile, content, (err) => {
        if (err) {
          console.log(err);
          return;
        }
      });
      fs.readFile(data[1], "utf-8", (err, content) => {
        if (err) {
          console.log(err);
          return;
        } else {
          content = content.split(" ");
          content.sort((a, b) => {
            if (a > b) return 1;
            else if (a < b) return -1;
            else return 0;
          });
          content = content.toString();
          fs.appendFile(sortFile, content, (err) => {
            if (err) {
              console.log(err);
              return err;
            } else {
              console.log(`Created ${sortFile} Successfully`);
              fs.appendFile("filenames.txt", "\n" + sortFile, (err) => {
                if (err) {
                  console.log(err);
                  return err;
                } else {
                  console.log("sortedFile name Added in Filenames.txt");
                  fs.readFile("./filenames.txt", "utf-8", (err, filedata) => {
                    if (err) {
                      console.log(err);
                      return;
                    } else {
                      
                      filedata = filedata.split("\n");
                      filedata.forEach((file) => {
                        fs.unlink(file, (err) => {
                          if (err) {
                            console.log("File Aready Deleted");
                          } else {
                            console.log(`${file} deleted Successfully`);
                          }
                        });
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports.readAndRight = readAndRight;
module.exports.convertUpper = convertUpper;
module.exports.convertLower = convertLower;
module.exports.sortFile = sortFile;
