const fs = require("fs");

function makeFiles(directory, filesCount, callback) {
  fs.mkdir(directory, (err) => {
    if (err) {
      console.log(err);
      return;
    } else {
       console.log(`Directory Added Successfully`);
      for (let i = 1; i <= filesCount; i++) {
        const data = Math.random();
        fs.writeFile(
          `./${directory}/file${i}.json`,
          JSON.stringify(data),
          (err) => {
            if (err) {
              console.log(err);
              return;
            } else {
              console.log(`File${i}.js Added Successfully`);
            }
          }
        );
      }
      callback();
    }
  });
}

function removeFiles(directory, filesCount, callback) {
  fs.readdir(directory, (err, files) => {
    if (err) {
      console.log(err);
      return;
    } else {
      let size = 1;
      files.forEach((file) => {
        const location = `${directory}/${file}`;
        fs.unlink(location, (err) => {
          if (err) {
            console.log(err);
            return;
          } else {
            console.log(`${file}.js Deleted Successfully`);
            size++;
            if (size == filesCount) {
              fs.rmdir(directory, (err) => {
                if (err) {
                  console.log(err);
                  return;
                } else {
                  console.log(`Directory Deleted Successfully`);
                  callback();
                }
              });
            }
          }
        });
      });
    }
  });
}

module.exports.makeFiles = makeFiles;
module.exports.removeFiles = removeFiles;

